#include <string>
#include <stdexcept>

using namespace std;

namespace hamming {
  int compute(string s1, string s2) {
    // Check lengths are the same
    if (s1.length() != s2.length())
      throw domain_error("Bleh");

    int res = 0;
    auto i = begin(s1);
    auto j = begin(s2);
    while (i < end(s1)) {
      if (*i != *j) {
	res++;
      }
      i++;
      j++;
    }
    return res;
  }
}
