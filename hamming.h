#if !defined(HAMMING_H)
#define HAMMING_H

#include <string>

using namespace std;

namespace hamming {
  int compute(string s1, string s2);
}

#endif
